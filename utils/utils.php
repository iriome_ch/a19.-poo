<?php
function selectPartner($partners)
{
    if (count($partners) <= 3) {
        return $partners;
    } else {
        shuffle($partners);

        return array_slice($partners, 0, 3);
    }
}
