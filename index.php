<?php

include __DIR__ . "/utils/utils.php";

require "entity/ImagenGaleria.php";
require "entity/Partner.php";

$images = array();

for ($i = 0; $i < 12; $i++) {

    $images[$i] = new ImagenGaleria(($i + 1) . ".jpg", "descipcion imagen " . ($i + 1), rand(100, 1000000000), rand(100, 1000000000), rand(100, 1000000000));
}

$partners = array();

for ($i = 0; $i < 11; $i++) {

    $partners[$i] = new Partner("Partner " . ($i), "./images/index/log3.jpg", "Texto alternativo de partner " . $i);
}

require "views/index.views.php";
