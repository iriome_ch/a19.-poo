<?php

class ImagenGaleria
{
    const R_I_PORTFOLIO = "images/index/portfolio/";
    const R_I_GALERIA = "images/index/gallery/";
    private $name;
    private $descripcion;

    private $n_Views;

    private $n_likes;

    private $n_downloads;

    public function __construct($name, $descripcion, $n_Views = 0, $n_likes = 0, $n_downloads = 0)
    {
        $this->name = $name;
        $this->descripcion = $descripcion;
        $this->n_Views = $n_Views;
        $this->n_likes = $n_likes;
        $this->n_downloads = $n_downloads;
    }
    /**
     * Get the value of n_downloads
     */
    public function getNDownloads()
    {
        return $this->n_downloads;
    }

    /**
     * Set the value of n_downloads
     */
    public function setNDownloads($n_downloads): self
    {
        $this->n_downloads = $n_downloads;

        return $this;
    }

    /**
     * Get the value of n_likes
     */
    public function getNLikes()
    {
        return $this->n_likes;
    }

    /**
     * Set the value of n_likes
     */
    public function setNLikes($n_likes): self
    {
        $this->n_likes = $n_likes;

        return $this;
    }

    /**
     * Get the value of n_Views
     */
    public function getNViews()
    {
        return $this->n_Views;
    }

    /**
     * Set the value of n_Views
     */
    public function setNViews($n_Views): self
    {
        $this->n_Views = $n_Views;

        return $this;
    }

    /**
     * Get the value of descripcion
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     */
    public function setDescripcion($descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    public function __ToString()
    {
        return $this->getDescripcion();
    }

    public function getURLPortfolio()
    {
        return self::R_I_PORTFOLIO . $this->getName();
    }

    public function getURLGaleria()
    {
        return self::R_I_GALERIA . $this->getName();
    }
}
